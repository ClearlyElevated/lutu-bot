### v1.0.0

---


> `Fixed:` Scan Command

> `Fixed:` Setup Command

> `Fixed:` All Malformed Emojis

---


> `Added:` Abality To Update Settings Without Using Dashboard


---